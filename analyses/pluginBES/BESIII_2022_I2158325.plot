BEGIN PLOT /BESIII_2022_I2158325/d01-x01-y01
LogY=0
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_{\Sigma^+}$
XLabel=$\cos\theta_{\Sigma^+}$
Title=$\cos\theta_{\Sigma^+}$ distribution for $\psi(2S)\to\Sigma^+\bar{\Sigma}^-$
LegendXPos=0.4
END PLOT
BEGIN PLOT /BESIII_2022_I2158325/d02-x01-y01
YLabel=$\alpha_{\psi(2S)}$
Title=$\alpha_{\psi(2S)}$ for $\psi(2S)\to\Sigma^-\bar{\Sigma}^+$
LogY=0
LegendYPos=0.4
END PLOT
