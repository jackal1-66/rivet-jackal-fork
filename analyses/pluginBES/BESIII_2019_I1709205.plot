BEGIN PLOT /BESIII_2019_I1709205/
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1709205/d01-x01-y01
Title=$p\eta^\prime$ mass distribution in $\psi(2S)\to p\bar{p}\eta^\prime$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{p\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1709205/d01-x01-y02
Title=$\bar{p}\eta^\prime$ mass distribution in $\psi(2S)\to p\bar{p}\eta^\prime$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{\bar{p}\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1709205/d01-x01-y03
Title=$p\bar{p}$ mass distribution in $\psi(2S)\to p\bar{p}\eta^\prime$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{p\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1709205/d01-x01-y04
Title=$p\eta^\prime$ mass distribution in $\psi(2S)\to p\bar{p}\eta^\prime$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
XLabel=$m_{p\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1709205/d01-x01-y05
Title=$\bar{p}\eta^\prime$ mass distribution in $\psi(2S)\to p\bar{p}\eta^\prime$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
XLabel=$m_{\bar{p}\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1709205/d01-x01-y06
Title=$p\bar{p}$ mass distribution in $\psi(2S)\to p\bar{p}\eta^\prime$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
XLabel=$m_{p\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1709205/dalitz_1
Title=Dalitz plot for  $\psi(2S)\to p\bar{p}\eta^\prime$
XLabel=$m^2_{p\eta^\prime}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{p}\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{p\eta^\prime}/{\rm d}m^2_{\bar{p}\eta^\prime}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1709205/d02-x01-y01
Title=$p\eta^\prime$ mass distribution in $J/\psi\to p\bar{p}\eta^\prime$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{p\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1709205/d02-x01-y02
Title=$\bar{p}\eta^\prime$ mass distribution in $J/\psi\to p\bar{p}\eta^\prime$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{\bar{p}\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1709205/d02-x01-y03
Title=$p\bar{p}$ mass distribution in $J/\psi\to p\bar{p}\eta^\prime$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{p\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1709205/d02-x01-y04
Title=$p\eta^\prime$ mass distribution in $J/\psi\to p\bar{p}\eta^\prime$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
XLabel=$m_{p\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1709205/d02-x01-y05
Title=$\bar{p}\eta^\prime$ mass distribution in $J/\psi\to p\bar{p}\eta^\prime$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
XLabel=$m_{\bar{p}\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1709205/d02-x01-y06
Title=$p\bar{p}$ mass distribution in $J/\psi\to p\bar{p}\eta^\prime$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
XLabel=$m_{p\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1709205/dalitz_2
Title=Dalitz plot for  $J/\psi\to p\bar{p}\eta^\prime$
XLabel=$m^2_{p\eta^\prime}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{p}\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{p\eta^\prime}/{\rm d}m^2_{\bar{p}\eta^\prime}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
