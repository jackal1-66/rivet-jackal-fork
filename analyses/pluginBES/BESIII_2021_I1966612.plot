BEGIN PLOT /BESIII_2021_I1966612/d01-x01-y01
Title=$\sigma(e^+e^-\to n\bar{n})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to n\bar{n})$/pb
ConnectGaps=1
END PLOT
