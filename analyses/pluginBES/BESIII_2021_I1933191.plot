BEGIN PLOT /BESIII_2021_I1933191/d01-x01-y01
Title=$\sigma(e^+e^-\to \pi^+\pi^-\psi(2S))$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-\psi(2S))$/pb
ConnectGaps=1
END PLOT
