Name: BESIII_2021_I1996394
Year: 2021
Summary: Measurement of $R$ for energies between 2.2324 to 3.6710 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1996394
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2112.11728
RunInfo: e+ e- to hadrons and e+ e- to mu+ mu- (for normalization)
NeedCrossSection: yes
Beams: [e-, e+]
Description:
  'Measurement of $R$ and the hadronic cross section in $e^+e^-$ collisions by BESIII for energies between 2.2324 to 3.6710 GeV.
   The muonic cross section is also outputted to the yoda file
   so that ratio $R$ can be recalculated if runs are combined.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021wib
BibTeX: '@article{BESIII:2021wib,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Measurement of the Cross Section for $e^{+}e^{-}\to$ hadrons at Energies from 2.2324 to 3.6710 GeV}",
    eprint = "2112.11728",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "12",
    year = "2021"
}'
