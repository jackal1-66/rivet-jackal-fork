BEGIN PLOT /CLEO_2008_I787608/
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d01-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $\chi_{c0}\to \pi^+\pi^-\pi^0\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d01-x01-y02
Title=$\pi^-\pi^0$ mass distribution in $\chi_{c0}\to \pi^+\pi^-\pi^0\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d02-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $\chi_{c1}\to \pi^+\pi^-\pi^0\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d02-x01-y02
Title=$\pi^-\pi^0$ mass distribution in $\chi_{c1}\to \pi^+\pi^-\pi^0\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d03-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $\chi_{c2}\to \pi^+\pi^-\pi^0\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d03-x01-y02
Title=$\pi^-\pi^0$ mass distribution in $\chi_{c2}\to \pi^+\pi^-\pi^0\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /CLEO_2008_I787608/d04-x01-y01
Title=$K^\pm\pi^\mp$ mass distribution in $\chi_{c0}\to K^\pm\pi^\mp K^0_S\pi^0$
XLabel=$m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d04-x01-y02
Title=$\pi^\pm\pi^0$ mass distribution in $\chi_{c0}\to K^\pm\pi^\mp K^0_S\pi^0$
XLabel=$m_{\pi^\pm\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^\pm\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d04-x01-y03
Title=$K^\pm\pi^0$ mass distribution in $\chi_{c0}\to K^\pm\pi^\mp K^0_S\pi^0$
XLabel=$m_{K^\pm\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^\pm\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d04-x01-y04
Title=$K^0_S\pi^\pm$ mass distribution in $\chi_{c0}\to K^\pm\pi^\mp K^0_S\pi^0$
XLabel=$m_{K^0_S\pi^\pm}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\pi^\pm}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /CLEO_2008_I787608/d05-x01-y01
Title=$K^\pm\pi^\mp$ mass distribution in $\chi_{c1}\to K^\pm\pi^\mp K^0_S\pi^0$
XLabel=$m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d05-x01-y02
Title=$\pi^\pm\pi^0$ mass distribution in $\chi_{c1}\to K^\pm\pi^\mp K^0_S\pi^0$
XLabel=$m_{\pi^\pm\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^\pm\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /CLEO_2008_I787608/d06-x01-y01
Title=$K^\pm\pi^\mp$ mass distribution in $\chi_{c2}\to K^\pm\pi^\mp K^0_S\pi^0$
XLabel=$m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d06-x01-y02
Title=$\pi^\pm\pi^0$ mass distribution in $\chi_{c2}\to K^\pm\pi^\mp K^0_S\pi^0$
XLabel=$m_{\pi^\pm\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^\pm\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d06-x01-y03
Title=$K^\pm\pi^0$ mass distribution in $\chi_{c2}\to K^\pm\pi^\mp K^0_S\pi^0$
XLabel=$m_{K^\pm\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^\pm\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d06-x01-y04
Title=$K^0_S\pi^\pm$ mass distribution in $\chi_{c2}\to K^\pm\pi^\mp K^0_S\pi^0$
XLabel=$m_{K^0_S\pi^\pm}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\pi^\pm}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2008_I787608/d06-x01-y05
Title=$K^0_S\pi^0$ mass distribution in $\chi_{c2}\to K^\pm\pi^\mp K^0_S\pi^0$
XLabel=$m_{K^0_S\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
