BEGIN PLOT /CMS_2022_I2129461/d06-x01-y08
Title=CMS, 13$\,$TeV, $\mathrm{tW}$, $\mathrm{e}^\pm\mu^\mp + \mathrm{1j1b+0j}_{l}$
XLabel=Leading lepton $p_{\mathrm{T}}$ (GeV)
YLabel=$(1/\sigma_{\mathrm{fid.}})d\sigma/d(\mathrm{leading lepton}\,p_{\mathrm{T}})$ (1/GeV)
YMin=0
YMax=0.035
RatioPlotYMin=0.5
RatioPlotYMax=2.5
LogY=0
END PLOT

BEGIN PLOT /CMS_2022_I2129461/d07-x01-y08
Title=CMS, 13$\,$TeV, $\mathrm{tW}$, $\mathrm{e}^\pm\mu^\mp + \mathrm{1j1b+0j}_{l}$
XLabel=$p_{\mathrm{Z}} (e^{\pm},\,\mu^{\mp},\,j)$ (GeV)
YLabel=$(1/\sigma_{\mathrm{fid.}})d\sigma/d(p_{\mathrm{Z}}\,(e^{\pm},\,\mu^{\mp},\,j))$ (1/GeV)
YMin=0.0
YMax=0.005
RatioPlotYMin=0.5
RatioPlotYMax=1.5
LogY=0
END PLOT

BEGIN PLOT /CMS_2022_I2129461/d08-x01-y08
Title=CMS, 13$\,$TeV, $\mathrm{tW}$, $\mathrm{e}^\pm\mu^\mp + \mathrm{1j1b+0j}_{l}$
XLabel=Jet $p_{\mathrm{T}}$ (GeV)
YLabel=$(1/\sigma_{\mathrm{fid.}})d\sigma/d(\mathrm{jet}\,p_{\mathrm{T}})$ (1/GeV)
YMin=0.0
YMax=0.026
RatioPlotYMin=0.5
RatioPlotYMax=1.5
LogY=0
END PLOT

BEGIN PLOT /CMS_2022_I2129461/d09-x01-y08
Title=CMS, 13$\,$TeV, $\mathrm{tW}$, $\mathrm{e}^\pm\mu^\mp + \mathrm{1j1b+0j}_{l}$
XLabel=$m(e^{\pm},\,\mu^{\mp},\,j)$ (GeV)
YLabel=$(1/\sigma_{\mathrm{fid.}})d\sigma/d(#m(e^{\pm}, \mu^{\mp}, j)$ (1/GeV)
YMin=0.0
YMax=0.011
RatioPlotYMin=0.5
RatioPlotYMax=1.5
LogY=0
END PLOT

BEGIN PLOT /CMS_2022_I2129461/d10-x01-y08
Title=CMS, 13$\,$TeV, $\mathrm{tW}$, $\mathrm{e}^\pm\mu^\mp + \mathrm{1j1b+0j}_{l}$
XLabel=$\Delta\varphi(e^{\pm},\,\mu^{\mp})/\pi$
YLabel=$(1/\sigma_{\mathrm{fid.}})d\sigma/d(\Delta\varphi(e^{\pm},\,\mu^{\mp})/\pi)$
YMin=0.0
YMax=1.55
RatioPlotYMin=0.5
RatioPlotYMax=1.5
LogY=0
END PLOT

BEGIN PLOT /CMS_2022_I2129461/d11-x01-y08
Title=CMS, 13$\,$TeV, $\mathrm{tW}$, $\mathrm{e}^\pm\mu^\mp + \mathrm{1j1b+0j}_{l}$
XLabel=$m_{\mathrm{T}}(e^{\pm},\,\mu^{\mp},\,\vec{p}_{\mathrm{T}}^{\mathrm{miss}},\,j)$ (GeV)
YLabel=$(1/\sigma_{fid.})d\sigma/d(m_{\mathrm{T}}(e^{\pm},\,\mu^{\mp},\,\vec{p}_{\mathrm{T}}^{\mathrm{miss}},\,j))$ (1/GeV)
YMin=0.0
YMax=0.0105
RatioPlotYMin=0.5
RatioPlotYMax=1.5
LogY=0
END PLOT
