Name: BELLE_2021_I1748231
Year: 2021
Summary: $B\to K\ell^+\ell^-$ decays
Experiment: BELLE
Collider: KEKB
InspireID: 1748231
Status: VALIDATED NOHEPDATA SINGLEWEIGHT
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 03 (2021) 105
RunInfo: Any process producing B0 and B+, original Upsilon(4S) decays
Description:
  'Measurement of the flavour separated differential branching ratio and asymmetries in $B\to K\ell^+\ell^-$ decays.'
ValidationInfo:
  'Herwig 7 events using EvtGen for decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BELLE:2019xld
BibTeX: '@article{BELLE:2019xld,
    author = "Choudhury, S. and others",
    collaboration = "BELLE",
    title = "{Test of lepton flavor universality and search for lepton flavor violation in $B \rightarrow K\ell \ell$ decays}",
    eprint = "1908.01848",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "BELLE-CONF-1904, Belle Preprint 2020-11, KEK Preprint 2020-12",
    doi = "10.1007/JHEP03(2021)105",
    journal = "JHEP",
    volume = "03",
    pages = "105",
    year = "2021"
}
'
