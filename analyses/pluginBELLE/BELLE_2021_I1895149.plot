BEGIN PLOT /BELLE_2021_I1895149/
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1895149/d01-x01-y01
Title=Lepton Energy in $B\to X_u\ell^+\nu_\ell$
XLabel=$E^B_\ell$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}E^B_\ell$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1895149/d02-x01-y01
Title=$q^2$ in $B\to X_u\ell^+\nu_\ell$
XLabel=$q^2$ [$\text{GeV}^2$]
YLabel=$\text{d}\mathcal{B}/\text{d}q^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1895149/d03-x01-y01
Title=Hadronic Mass in $B\to X_u\ell^+\nu_\ell$
XLabel=$M_X$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}M_X$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1895149/d04-x01-y01
Title=Hadronic Mass Squared in $B\to X_u\ell^+\nu_\ell$
XLabel=$M_X^2$ [$\text{GeV}^2$]
YLabel=$\text{d}\mathcal{B}/\text{d}M^2_X$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1895149/d05-x01-y01
Title=Positive Light Cone Momentum in $B\to X_u\ell^+\nu_\ell$
XLabel=$P_+$ [$\text{GeV}$]
YLabel=$\text{d}\mathcal{B}/\text{d}P_+$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1895149/d06-x01-y01
Title=Negative Light Cone Momentum in $B\to X_u\ell^+\nu_\ell$
XLabel=$P_-$ [$\text{GeV}$]
YLabel=$\text{d}\mathcal{B}/\text{d}P_-$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2021_I1895149/d07-x01-y01
Title=Lepton Energy in $B\to X_u\ell^+\nu_\ell$
XLabel=$E^B_\ell$ [GeV]
YLabel=$N$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1895149/d08-x01-y01
Title=$q^2$ in $B\to X_u\ell^+\nu_\ell$
XLabel=$q^2$ [$\text{GeV}^2$]
YLabel=$N$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1895149/d09-x01-y01
Title=Hadronic Mass in $B\to X_u\ell^+\nu_\ell$
XLabel=$M_X$ [GeV]
YLabel=$N$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1895149/d10-x01-y01
Title=Hadronic Mass Squared in $B\to X_u\ell^+\nu_\ell$
XLabel=$M_X^2$ [$\text{GeV}^2$]
YLabel=$N$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1895149/d11-x01-y01
Title=Positive Light Cone Momentum in $B\to X_u\ell^+\nu_\ell$
XLabel=$P_+$ [$\text{GeV}$]
YLabel=$N$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1895149/d12-x01-y01
Title=Negative Light Cone Momentum in $B\to X_u\ell^+\nu_\ell$
XLabel=$P_-$ [$\text{GeV}$]
YLabel=$N$
LogY=0
END PLOT
