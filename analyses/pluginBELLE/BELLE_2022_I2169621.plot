BEGIN PLOT /BELLE_2022_I2169621/d01-x01-y01
Title=$B^0\to D^-e^+\nu$
XLabel=$w$
YLabel=$\text{d}\Gamma/\text{d}w\times10^{15}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2022_I2169621/d01-x01-y02
Title=$B^0\to D^-\mu^+\nu$
XLabel=$w$
LogY=0
YLabel=$\text{d}\Gamma/\text{d}w\times10^{15}$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2022_I2169621/d01-x01-y03
Title=$B^+\to D^0e^+\nu$
XLabel=$w$
LogY=0
YLabel=$\text{d}\Gamma/\text{d}w\times10^{15}$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2022_I2169621/d01-x01-y04
Title=$B^+\to D^0\mu^+\nu$
XLabel=$w$
LogY=0
YLabel=$\text{d}\Gamma/\text{d}w\times10^{15}$ [GeV]
END PLOT
