BEGIN PLOT /BELLE_2021_I1859137/d01-x01-y01
Title=Cross Section for $e^+e^-\to B\bar{B}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BELLE_2021_I1859137/d01-x01-y02
Title=Cross Section for $e^+e^-\to B\bar{B}^*$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BELLE_2021_I1859137/d01-x01-y03
Title=Cross Section for $e^+e^-\to B^*\bar{B}*^$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
LogY=0
ConnectGaps=1
END PLOT
