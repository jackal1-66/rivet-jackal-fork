BEGIN PLOT /HRS_1985_I213242/d01-x01-y01
Title=Scaled energy for $\phi$ at 29 GeV
XLabel=$x_E$
YLabel=$s/\beta\mathrm{d}\sigma/\mathrm{d}x_E$ [$\mathrm{nb}\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /HRS_1985_I213242/d02-x01-y01
Title=Scaled energy for $D_s^\pm$ at 29 GeV
XLabel=$x_E$
YLabel=$\text{BR}(D_s^+\to\phi\pi^+)s/\beta\mathrm{d}\sigma/\mathrm{d}x_E$ [$\mathrm{nb}\mathrm{GeV}^2$]
END PLOT
