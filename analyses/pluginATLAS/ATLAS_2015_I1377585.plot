BEGIN PLOT /ATLAS_2015_I1377585/d01-x01-y01
Title=Fiducial Cross Section for $e^+e^-$ production
YLabel=$\sigma$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ATLAS_2015_I1377585/d01-x01-y02
Title=Fiducial Cross Section for $\mu^+\mu^-$ production
YLabel=$\sigma$ [pb]
LogY=0
END PLOT
