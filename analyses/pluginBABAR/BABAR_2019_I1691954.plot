BEGIN PLOT /BABAR_2019_I1691954/d01-x01-y01
Title=$e^+e^-$ mass distribution in $D^0\to K^-\pi^+ e^+e^-$ ($0.675<m_{ee}<0.875\text{GeV}$)
XLabel=$m^2_{e^+e^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{e^+e^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2019_I1691954/d01-x01-y02
Title=$K^-\pi^+$ mass distribution in $D^0\to K^-\pi^+ e^+e^-$ ($0.675<m_{ee}<0.875\text{GeV}$)
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2019_I1691954/d02-x01-y01
Title=$e^+e^-$ mass distribution in $D^0\to K^-\pi^+ e^+e^-$ ($m_{ee}>0.2\text{GeV}$)
XLabel=$m^2_{e^+e^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{e^+e^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
