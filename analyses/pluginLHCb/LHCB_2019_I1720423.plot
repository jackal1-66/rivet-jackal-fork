BEGIN PLOT /LHCB_2019_I1720423/d01-x01-y01
Title=$K^+K^-$ mass distribution in $D^+\to K^+K^+K^-$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /LHCB_2019_I1720423/d01-x01-y02
Title=$K^+K^+$ mass distribution in $D^+\to K^+K^+K^-$
XLabel=$m^2_{K^+K^+}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2019_I1720423/d01-x01-y03
Title=Higher $K^+K^-$ mass distribution in $D^+\to K^+K^+K^-$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2019_I1720423/d01-x01-y04
Title=Lower $K^+K^-$ mass distribution in $D^+\to K^+K^+K^-$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2019_I1720423/dalitz
Title=Dalitz plot for $D^+\to K^+K^+K^-$
XLabel=$m^2_{{\rm low}K^+K^-}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{{\rm high}K^+K^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{{\rm low}K^+K^-}/{\rm d}m^2_{{\rm high}K^+K^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
