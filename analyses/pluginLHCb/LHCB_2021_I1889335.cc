// -*-C++ - *-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/AliceCommon.hh"

namespace Rivet
{

  /// @brief Charged particle production at 13 TeV
  class LHCB_2021_I1889335 : public Analysis
  {
  public:
    /// Constructor
    RIVET_DEFAULT_ANALYSIS_CTOR(LHCB_2021_I1889335);

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init()
    {

      // Register projection for primary particles
      declare(ALICE::PrimaryParticles(Cuts::etaIn(ETAMIN, ETAMAX) && Cuts::abscharge > 0), "APRIM");

      {Histo1DPtr tmp; _h_ppInel_neg.add(2.0, 2.5, book(tmp, 1, 1, 1));}
      {Histo1DPtr tmp; _h_ppInel_neg.add(2.5, 3.0, book(tmp, 1, 1, 2));}
      {Histo1DPtr tmp; _h_ppInel_neg.add(3.0, 3.5, book(tmp, 1, 1, 3));}
      {Histo1DPtr tmp; _h_ppInel_neg.add(3.5, 4.0, book(tmp, 1, 1, 4));}
      {Histo1DPtr tmp; _h_ppInel_neg.add(4.0, 4.5, book(tmp, 1, 1, 5));}
      {Histo1DPtr tmp; _h_ppInel_neg.add(4.5, 4.8, book(tmp, 1, 1, 6));}
      
      {Histo1DPtr tmp; _h_ppInel_pos.add(2.0, 2.5, book(tmp, 2, 1, 1));}
      {Histo1DPtr tmp; _h_ppInel_pos.add(2.5, 3.0, book(tmp, 2, 1, 2));}
      {Histo1DPtr tmp; _h_ppInel_pos.add(3.0, 3.5, book(tmp, 2, 1, 3));}
      {Histo1DPtr tmp; _h_ppInel_pos.add(3.5, 4.0, book(tmp, 2, 1, 4));}
      {Histo1DPtr tmp; _h_ppInel_pos.add(4.0, 4.5, book(tmp, 2, 1, 5));}
      {Histo1DPtr tmp; _h_ppInel_pos.add(4.5, 4.8, book(tmp, 2, 1, 6));}
    }

    void analyze(const Event &event)
    {

      const Particles cfs = apply<ALICE::PrimaryParticles>(event, "APRIM").particles();

      for (const Particle& myp : cfs)
      {
        if (myp.charge() < 0)
        {
          _h_ppInel_neg.fill(myp.pseudorapidity(), myp.momentum().pT());
        }
        else
        {
          _h_ppInel_pos.fill(myp.pseudorapidity(), myp.momentum().pT());
        }
      }
    }

    /// Normalise histograms etc., after the run
    void finalize()
    {
      const double scale_factor = crossSection() / millibarn / sumOfWeights();
      std::vector<double> binWidths = {0.5, 0.5, 0.5, 0.5, 0.5, 0.3};
      for (size_t i = 0; i < binWidths.size(); i++)
      {
        _h_ppInel_neg.histos()[i]->scaleW(scale_factor / binWidths[i]);
        _h_ppInel_pos.histos()[i]->scaleW(scale_factor / binWidths[i]);
      }
    }

    /// @name Histogram
    BinnedHistogram _h_ppInel_neg;
    BinnedHistogram _h_ppInel_pos;

    /// Cut constants
    const double ETAMIN = 2.0, ETAMAX = 4.8;
  };

  RIVET_DECLARE_PLUGIN(LHCB_2021_I1889335);
}